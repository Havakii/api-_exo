fetch('https://api.chucknorris.io/jokes/random')
    .then(function(resultat){
        console.log(resultat)
        return resultat.json();
    })

    .then(function(jokes){
        console.log(jokes);
        const randomJoke = jokes;
       
        document.querySelector("p").innerText = randomJoke.value;
    
    });


    fetch('https://dog.ceo/api/breeds/image/random')
    .then(function(resultat){
        console.log(resultat)
        return resultat.json();
    })

    .then(function(dog){
        console.log(dog);
        const randomDog = dog;
       
        document.getElementById("doggy").src = randomDog.message;
    
    });

    fetch('https://thatcopy.pw/catapi/rest/')
    .then(function(resultat){
        console.log(resultat)
        return resultat.json();
    })

    .then(function(cat){
        console.log(cat);
        const randomCat = cat;
       
        document.getElementById("kitty").src = randomCat.url;
    
    });
    